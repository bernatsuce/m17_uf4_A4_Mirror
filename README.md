Risketos Mínims

Client i servidor clarament separats i diferenciats 

Comandes úniques de client i comandes úniques de servidor

Mínim de 2 variables sincronitzades, amb els seus hooks

Ha d’haver missatges de client a servidor per a que executi (commands) i missatges de servidor a client per a que executi (ClientRpc o TargetRpc)

Sistema de Comunicació client-client. Xat, emoticones, frases predefinides, etc

Canvi d’escenes amb un sistema de lobby

Mínim de 2 Objectes spawnables degudament registrats





Risketos Addicionals

Sincronització de temps mitjançant NetworkTime

Ús del RoomManager
